#!/usr/bin/env python3

from distutils.core import setup

setup(
    name = '{{cookiecutter.name}}',
    version = '{{cookiecutter.version}}',
    description = '{{cookiecutter.description}}',
    author = '{{cookiecutter.author}}',
    author_email = '{{cookiecutter.email}}',
    url = '{{cookiecutter.url}}',
    packages = ['{{cookiecutter.name}}'],
    package_dir = {'{{cookiecutter.name}}': 'source/{{cookiecutter.name}}'},
)

